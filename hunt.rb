# encoding: utf-8
require 'rubygems'
require 'mechanize'
require 'bing_translator'

@translator = BingTranslator.new('TestowaApka', 'o/8BBa3/qu0S5ZrXJtdRYOzw/Z1rBcla7RDXPwgFFp8=')

@last_offer    = {}
@current_offer = {}
@greetings = [
  "Oh, look at Adrian, he is mr. fishman number one",
  "What time it is?",
  "It is time for Doughnut",
  "Code review anyone",
  "Code review time guys",
  "Please do code review urgently",
  "Stop playing around and get back to work!",
  "What are you looking for Majk Orzol?",
  "This is time for Code review!",
  "Oh no. Adrian is waiting for code revie!",
  "Dont forget about dinner today guys!",
  "Come on! You should deliver this task today!"
]
@stop = true

loop do
  print 'x'
  @agent = Mechanize.new
  @page                  = @agent.get("http://www.ibood.com/pl/pl/")
  @current_offer         = {
    name: @page.search("#link_product").text(),
    price: @page.search(".prices .price").text().strip
  }

  if @current_offer[:name] != @last_offer[:name]
    translated_title = @translator.translate @current_offer[:name], from: 'pl', to: 'en'
    puts "[!] #{Time.now} Ibood HUNT: #{@current_offer.inspect}"
    system("say 'NEW HUNT OFFER!: #{translated_title}, price #{@current_offer[:price].to_s.gsub('zł', 'PLN')}'")
  end

  system("say '#{@greetings[rand(@greetings.size)]}'") if @stop and rand(666)%17 == 0

  3.times {|x| print '.' ; sleep 1;}
  @stop = !@stop 
  @last_offer = @current_offer
end